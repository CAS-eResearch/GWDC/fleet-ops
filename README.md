# Fleet Operations

This repository is intended for applying [Fleet Infrastructure](https://gitlab.com/CAS-eResearch/GWDC/fleet-infra) application runtime configuration.

## Getting Started

```bash
#!/bin/bash

# Validate argocd deployment(s)
kubectl -n $NAMESPACE get pods

# Get credentials
kubectl -n argocd get secret argocd-initial-admin-secret -o yaml | grep -o 'password: .*' | sed -e s"/password\: //g" | base64 -d

# Proxy the argocd-server
kubectl -n $NAMESPACE port-forward svc/argocd-server -n argocd 8080:443

# Login to argocd
argocd login localhost:8080

# Self manage the argocd app
argocd app create fleet-ops \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/CAS-eResearch/GWDC/fleet-ops.git \
    --path apps  
argocd app sync fleet-ops
```

## Applications in Scope

TBA
